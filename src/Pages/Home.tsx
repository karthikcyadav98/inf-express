import React from 'react'
import Menu1 from '../assets/menu1.jpg'
import Menu2 from '../assets/menu2.jpg'
import Menu3 from '../assets/menu3.jpg'
import Header from '../common/Header'

const Home:React.FC = () => {
  return (
    <div>
      <Header/>
      
      <img alt='menu1' style={{width:"100%"}} src={Menu1} />
      <img alt='menu2' style={{width:"100%"}} src={Menu2} />
      <img alt='menu3' style={{width:"100%"}} src={Menu3} />
    </div>
  )
}

export default Home

import React from 'react';
import Logo from '../assets/logo.png'

const Header:React.FC = () => {
  return (
    <div
      style={{
        display:'flex',
        justifyContent:'center',
        alignItems:"center",        
      }}>
        <img style={{width:80, height:70}} src={Logo}  />
      <p>IHF EXPRESS</p>
    </div>
  );
};

export default Header;
